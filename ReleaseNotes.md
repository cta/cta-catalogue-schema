# v15.0

## Summary

### Catalogue Schema
- cta/CTA#738 - Add new repack tapepool field to the `archive_route` table
- cta/CTA#718 - Add IS_DISABLED, DISABLED_REASON columns to PHYSICAL_LIBRARY
- cta/CTA#598 - Add table `TAPE_POOL_SUPPLY` to model the tapepool `--supply` option
- cta/CTA#463 - Added new indexes to the FILE_RECYCLE_LOG catalogue table

### Other
- cta/CTA#809 - Replace CC7 for Alma9 in cta-catalogue-schema CI
- cta/cta-catalogue-schema#6 Remove obsolete catalogue schema PDF

# v14.0

## Summary

### Catalogue Schema
- cta/cta-catalogue-schema#4 - Remove unique from "CREATE UNIQUE INDEX LOGICAL_LIBRARY_PLI_IDX"

# v13.0

## Summary

**NOTE:** Version 13.0 has been removed due to a bug. Any migrations should be done directly from version 12.0 to 14.0.

### Catalogue Schema
- cta/CTA#31 - Allow VO override for repack
- cta/CTA#175 - Adding UPPERCASE constraint to TAPE VID
- cta/CTA#267 - Add new purchase order field to the tape table
- cta/CTA#276 - Add new physical tape library table to the CTA catalogue
- cta/CTA#276 - Fix typo "manfacturer" to manufacturer
- cta/CTA#236 - Start with the same values in both Oracle and Postgres sequences
- cta/CTA#351 - Fix syntax errors in rollback statements (DROP table to DROP TABLE table)
